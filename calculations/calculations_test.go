package calculations

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_find(t *testing.T) {
	slice := []string{
		"line1",
		"line2",
		"line3",
	}

	assert.True(t, find(slice, "line1"))
	assert.True(t, find(slice, "line2"))
	assert.True(t, find(slice, "line3"))
	assert.True(t, find(slice, "Line1"))
	assert.True(t, find(slice, "Line2"))
	assert.True(t, find(slice, "Line3"))

	assert.False(t, find(slice, "line2 "))
	assert.False(t, find(slice, "line"))
}

func Test_getPayersForPurchase(t *testing.T) {
	spendings := []*SpendingData{
		{
			Person: Person{"a1", ""},
			Purchases: []Purchase{
				{
					Name: "p1",
					Sum:  10,
				},
			},
		},
		{
			Person: Person{"a2", ""},
		},
		{
			Person: Person{"a3", ""},
			NoChipInFor: []string{
				"P1",
			},
		},
	}

	expected := []Person{
		{"a1", ""},
		{"a2", ""},
	}

	actual := getPayersForPurchase(spendings, "p1")

	assert.EqualValues(t, expected, actual)
}

func Test_aggregatePurchases(t *testing.T) {
	spendings := []*SpendingData{
		{
			Purchases: []Purchase{
				{"p1", 10},
				{"p3", 5},
			},
		},
		{
			Purchases: []Purchase{
				{"p2", 1},
				{"p3", 6},
				{"p1", 2},
			},
		},
		{
			Purchases: []Purchase{
				{"p1", 4},
			},
			NoChipInFor: []string{"p1"},
		},
	}

	expected := map[string]float64{
		"p1": float64(16),
		"p2": float64(1),
		"p3": float64(11),
	}

	actual := aggregatePurchases(spendings)

	assert.EqualValues(t, expected, actual)
}

func Test_CalcSum_v1(t *testing.T) {
	spendings := []*SpendingData{
		&SpendingData{
			Person: Person{"p1", ""},
			Purchases: []Purchase{
				Purchase{
					Name: "g1",
					Sum:  20,
				},
				Purchase{
					Name: "g2",
					Sum:  30,
				},
			},
		},
		&SpendingData{
			Person: Person{"p2", ""},
			NoChipInFor: []string{
				"g1",
				"g3",
			},
		},
		&SpendingData{
			Person: Person{"p3", ""},
			Purchases: []Purchase{
				{
					Name: "g3",
					Sum:  15,
				},
			},
			NoChipInFor: []string{"g3"},
		},
	}

	expected := []ChipInData{
		ChipInData{
			Person: Person{"p1", ""},
			Sum:    35,
		},
		ChipInData{
			Person: Person{"p2", ""},
			Sum:    10,
		},
		ChipInData{
			Person: Person{"p3", ""},
			Sum:    20,
		},
	}

	actual := CalcSum(spendings)
	sort.Slice(actual, func(i, j int) bool {
		return actual[i].Person.Name < actual[j].Person.Name
	})

	assert.EqualValues(t, expected, actual)
}
