package calculations

import (
	"fmt"
	"strings"
)

type Person struct {
	Name  string
	LName string
}

type Purchase struct {
	Name string
	Sum  float64
}

type SpendingData struct {
	Person      Person
	Purchases   []Purchase
	NoChipInFor []string
}

type ChipInData struct {
	Person Person
	Sum    float64
}

func CalcSum(spendings []*SpendingData) []ChipInData {
	chipInValues := map[string]ChipInData{}
	purchases := aggregatePurchases(spendings)

	for purchaseName, sum := range purchases {
		persons := getPayersForPurchase(spendings, purchaseName)
		individualSum := sum / float64(len(persons))

		for _, person := range persons {
			personKey := fmt.Sprintf("%s_%s", person.Name, person.LName)
			chipInValues[personKey] = ChipInData{
				Person: person,
				Sum:    individualSum + chipInValues[personKey].Sum,
			}
		}
	}

	data := []ChipInData{}
	for _, item := range chipInValues {
		data = append(data, item)
	}

	return data
}

func aggregatePurchases(spendings []*SpendingData) map[string]float64 {
	sumOfPurchaces := map[string]float64{}

	for _, spending := range spendings {
		for _, purchase := range spending.Purchases {
			name := strings.ToLower(purchase.Name)

			sumOfPurchaces[name] += purchase.Sum
		}
	}

	return sumOfPurchaces
}

func getPayersForPurchase(spendings []*SpendingData, purchaseName string) []Person {
	mappedPayers := map[string]Person{}

	for _, spending := range spendings {
		if !find(spending.NoChipInFor, purchaseName) {
			key := fmt.Sprintf("%s_%s", spending.Person.Name, spending.Person.LName)
			mappedPayers[key] = spending.Person
		}
	}

	payers := []Person{}
	for _, payer := range mappedPayers {
		payers = append(payers, payer)
	}

	return payers
}

func find(slice []string, line string) bool {
	for _, sliceLine := range slice {
		if strings.EqualFold(sliceLine, line) {
			return true
		}
	}

	return false
}
