package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/alex.dogonin/chip-in/calculations"
)

func main() {
	readFromStdIn := false
	filename := ""
	for _, value := range os.Args[1:] {
		if readFromStdIn = ("-" == value); !readFromStdIn {
			filename = value
		}

		break
	}

	if !readFromStdIn && "" == filename {
		pringUsage()
		os.Exit(1)
	}

	var jsonBytes []byte
	var err error

	if readFromStdIn {
		reader := bufio.NewReader(os.Stdin)

		if jsonBytes, err = reader.ReadBytes('\000'); nil != err && err != io.EOF {
			log.Fatalf("stdin read error, %v", err)
		}
	} else {
		if jsonBytes, err = ioutil.ReadFile(filename); err != nil {
			log.Fatalf("file read error, %v", err)
		}
	}

	spendings := []*calculations.SpendingData{}
	if err = json.Unmarshal(jsonBytes, &spendings); nil != err {
		log.Fatalf("unmarshal json error, %v", err)
	}

	chipInData := calculations.CalcSum(spendings)

	responseBytes, err := json.Marshal(chipInData)
	if nil != err {
		log.Fatalf("marshal response to json error, %v", err)
	}

	fmt.Printf("result:\n%s\n", responseBytes)
}

func pringUsage() {
	fmt.Println(`
Usage:

read from file:
chip-in <file_name>

read from stdin:
chil-in -
`)
}
